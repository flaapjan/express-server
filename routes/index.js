var express = require('express');
var router = express.Router();
var app = express();
var bodyParser = require('body-parser');
var uuid = require('uuid');
const MongoClient = require('mongodb').MongoClient;

const pass = 'hLEgmaFb40x6i0FO';
const url = `mongodb+srv://flaap:${pass}@cluster0-yobwp.mongodb.net/test`;
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});

let db;

MongoClient.connect(url, (err, database) => {
  if(err) {
    return console.log('MONGO connect error :: ', err);
  }
  db = database.db('query-service-passport');
  console.log(' ::>> db >>>> ', db);
  // start the express web server listening on 8080

});

app.post('/login', (req, res) => {
  console.log(' ::>> post >>>> ', req.body, req.body.username, req.body.password);

  try {

      db.collection('loginView').find(req.body).toArray((err, result) => {
          // db.collection('loginView').find(req.body).toArray((err, result) => {
      if (err) return console.log(err);
      if(result.length === 0) {
        res.status(404).send({});
        res.end('request successful : no content');
        return;
      }
      let response = result[0];
      delete response.password;
      res.status(200).send(response);
      res.end('request successful');
    });
  } catch(e) {
    console.log(' MONGO error >>>> ', e);
    res.status(400).send();
    res.end('request failed');
  }

  // res.status(202).send({token: uuid()});
  // res.end('request successful');
});

app.put('/register', (req, res) => {
  console.log(' ::>> register hit ');
  try {

    db.collection('loginView').find({ username: req.body.username }).toArray((err, result) => {
      console.log(' dATA >>>> ', {err, result});
      if (err) return console.log(err);
      if(result.length > 0) {
        res.status(400).send('Username must be unique.');
        res.end('request failed');
        return;
      }

      console.log(' ::>> result >>> ', req.body);
      db.collection('loginView').insertOne(req.body).catch((error) => {
        console.log(' ::>> an error occurred >>> ', error);
      });

      res.status(200).send(req.body);
      res.end('request successful');
    });
  } catch(e) {
    console.log(' MONGO error >>>> ', e);
    res.status(400).send({});
    res.end('request failed');
  }
});

app.post('/update', (req, res) => {
  console.log(' ::>> updating user ', req.body);

  try {
    db.collection('loginView').update({
      username: req.body.username
    }, { $set: req.body }).catch((err) => {
      console.error('failed to update user ', err);
      res.status(400).send({});
      res.end('request failed');
    });
    res.status(200).send(req.body);
    res.end('request successful');
  } catch(e) {
    res.status(400).send({});
    res.end('request failed');
  }
});

module.exports = app;





// app.get('/status', (req, res) => {
//   console.log(' hit query >>>> ', req);
//   // res.writeHead(200, {
//   //   'Content-Type': 'text/plain',
//   //   'Grip-Hold': 'stream',
//   //   'Grip-Channel': 'mychannel'
//   // });
//   // res.end('Stream opened, prepare yourself!\n');

//   // res.status(200).send(JSON.stringify(data[req.query.userid]));
//   // res.end('request successful');

//   // eventSource = res;

//   // const sendMessage = message => {
//   //   res.write('event: message\n' + `data: ${message}\n` + '\n');
//   //   res.flushHeaders();
//   // };

//   // try {
//   //   let _data = data[req.query.userid] || {};
//   //   sendMessage(JSON.stringify(_data));
//   //   //

//   //   // setTimeout(() => {
//   //   //   sendMessage(JSON.stringify({ routingStatus: 'Ready', presence: 'Available' }));
//   //   // }, 12000);
//   // } catch (e) {
//   //   console.log(' ::>> you broke something >>> ', e);
//   // }

//   res.status(200).send({ user: 'suppidy sup' });
//   res.end('request successful');
// });
